# Cherokee Language Utilities #



### What is this repository for? ###

* This project is the umbrella project for checking out and building Cherokee Language Utilies such as the transliteration engine, the conjugation utilities, and the conjugation engine
* Version 0.0.1

### How do I get set up? ###

* You will need Gradle installed - you can install it following directions at gradle.org
* You will need a Java version installed - anything over Java 8 should be fine
* All dependencies are included in the build.gradle files
* This project uses Groovy 3.0.6
* There is no database configuration needed
* Initial checkout and build run the init.sh script and the projects needed will be checked out and then built and installed for you
* to run tests or compile simply run the buildAll.sh script
* the build all script covers installation for each individual project - however, if you want to install one project only you simply go to that project then run 'gradle install'

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact